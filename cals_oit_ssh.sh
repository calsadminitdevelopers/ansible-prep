#!/bin/bash

#This script is used to create a cals_oit_ssh account
# Supported OSes - Ubuntu 16/18/20, CentOS/RHEL 7/8

#define variables
userName='cals_oit_ssh'
randPass=$(openssl rand -base64 128)
hashPass=$(echo $randPass | openssl passwd -1 -stdin)
sudoFile="/etc/sudoers.d/10_$userName"
sshPubKey="$userName"'_key.pub'
sshDir="/home/$userName/.ssh"
sshAuthKeys="$sshDir/authorized_keys"

# OS Detection
echo -n "Detecting OS... "
if [ -f /etc/redhat-release ]; then
  OS='RHEL'
elif (lsb_release -i | grep -q Ubuntu) ; then
  OS='Ubuntu'
else
  echo
  echo 'no supported OS detected.  Exiting...'
  exit
fi
echo $OS

#create cals_oit_ssh
useradd -m -s /bin/bash -p $hashPass $userName

#create visudo entry and verify it
echo "$userName ALL=(ALL) NOPASSWD:ALL" > $sudoFile && visudo -cf $sudoFile
chmod 440 $sudoFile

#install openssh
# installed by default on CentOS.  For Ubuntu, check
if [[ $OS == "Ubuntu" ]]; then
  if (dpkg-query --list openssh-server > /dev/null); then
    true
  else
    apt-get update && apt-get install -y openssh-server
  fi
fi

#copy public ssh key
if [ ! -e $sshDir ]; then
   mkdir $sshDir
   chown $userName:$userName $sshDir
   ( umask 077; touch $sshAuthKeys )
   chown $userName:$userName $sshAuthKeys
   cat $sshPubKey > $sshAuthKeys
else
   cat $sshPubKey > $sshAuthKeys
fi
